# CHART Authorship Network

**Purpose:** Generate a network diagram showing the authorship relationships among members of the CHART consortium. 

## Obtaining Citations

Data were provided by Lisa McElroy who downloaded it from PubMed with the following search string:

**Original query:**

Search: ((((((((((((((((((Schappe, Tyler[Author]) OR (Serper, Marina[Author])) OR (Strauss, Alexandra[Author])) OR (Ng, Yue Harn[Author])) OR (Schold, Jesse[Author])) OR (Adams, Andrew[Author])) OR (Caicedo, Juan[Author])) OR (Reed, Rhiannon[Author])) OR (Taber, David[Author])) OR (Gordon, Elisa[Author])) OR (Harding, Jessica[Author])) OR (Ross-Driscoll, Katherine[Author])) OR (Ross-Driscoll Katherine[Author])) OR (Patzer, Rachel[Author])) OR (Kirk, Allan[Author])) OR (Matsouaka, Roland[Author])) OR (Bhavsar Nrupen[Author])) OR (McElroy, Lisa[Author])) OR (McElroy Lisa[Author]) Sort by: Publication Date

**Modified query:** This can be pasted directly into PubMed search

(Schappe, Tyler[Author]) OR (Serper, Marina[Author]) OR (Strauss, Alexandra[Author]) OR (Ng, Yue Harn[Author]) OR (Schold, Jesse[Author]) OR (Adams, Andrew[Author]) OR (Caicedo, Juan[Author]) OR (Reed, Rhiannon[Author]) OR (Taber, David[Author]) OR (Gordon, Elisa[Author]) OR (Harding, Jessica[Author]) OR (Ross-Driscoll, Katherine[Author]) OR (Ross-Driscoll Katherine[Author]) OR (Patzer, Rachel[Author]) OR (Kirk, Allan[Author]) OR (Matsouaka, Roland[Author]) OR (Bhavsar Nrupen[Author]) OR (McElroy, Lisa[Author]) OR (McElroy Lisa[Author]) OR (Milam, AJ[Author]) OR (Locke, JE[Author])

## Updating

Steps to update the publication list:

1. Perform a PubMed search using the query above and download as a CSV to local computer
2. Compare the new and old datasets to check that things look good
3. Rename the file as "author_network_diagram_publication_list_latest.csv"
4. Overwrite the existing CSV file in the local repo directory (on DCC or elsewhere)
5. Open the existing app on the remote server and take a screen shot to compare against the new version to validate changes
6. Try running the app locally:
  1. Open /CHART_author_network/app.R
  2. Click 'Run App' near upper right corner of source code window to run the app -- it will display the graph in the plots pane
7. If all looks good, publish the app to the Shiny.io server by clicking the 'publish' button (blue circular arrows to the right of 'Run App') and push the app to the remote using the pop-up dialogue window.