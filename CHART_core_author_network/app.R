#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    https://shiny.posit.co/
#

library(shiny)

library(dplyr)
library(readr)
library(stringr)
library(foreach)
library(statnet)
library(network)
library(igraph)
library(visNetwork)

#Center-specific colors
color.colorado = "#CFB87C"
color.duke = "#012169"
color.emory = "#0c2340"
color.hopkins = "#68ACE5"
color.indiana = "#990000"
color.mayo = "#0E3293"
color.musc = "#01447A"
color.northwestern = "#4E2A84"
color.newmexico = "#ba0c2f"
color.penn = "#990000"
color.umn = "#7a0019"
color.vumc = "#FEEEB6"
color.uab = "#1E6B52"

#################
# Prepare Data
#################

# PubMed Query
# (Schappe, Tyler[Author]) OR (Serper, Marina[Author]) OR (Strauss, Alexandra[Author]) OR (Ng, Yue Harn[Author]) OR (Schold, Jesse[Author]) OR (Adams, Andrew[Author]) OR (Caicedo, Juan[Author]) OR (Reed, Rhiannon[Author]) OR (Taber, David[Author]) OR (Gordon, Elisa[Author]) OR (Harding, Jessica[Author]) OR (Ross-Driscoll, Katherine[Author]) OR (Ross-Driscoll Katherine[Author]) OR (Patzer, Rachel[Author]) OR (Kirk, Allan[Author]) OR (Matsouaka, Roland[Author]) OR (Bhavsar Nrupen[Author]) OR (McElroy, Lisa[Author]) OR (McElroy Lisa[Author]) OR (Milam, AJ[Author]) OR (Locke, JE[Author])

# Read data from public GitLab
pubs <- 
  readr::read_csv(file = "https://gitlab.oit.duke.edu/chart-consortium/authorship-network/-/raw/main/author_network_diagram_publication_list_latest.csv",
                  locale = readr::locale(encoding = (readr::guess_encoding(file = "https://gitlab.oit.duke.edu/chart-consortium/authorship-network/-/raw/main/author_network_diagram_publication_list_latest.csv") %>%
                                                       filter(confidence == max(confidence)) %>%
                                                       .$encoding)
                  )
  )
# Define primary authors
primary.authors <-
  c("Schappe T", 
    "Serper M", 
    "Strauss AT",
    "Ng YH",
    "Schold JD",
    "Adams AB",         #Verified on pubs page https://adamslab.umn.edu/2020-present-publications
    "Caicedo-Ramirez JC",
    "Reed RD", 
    "Taber DJ", 
    "Gordon EJ", 
    "Harding JF", 
    "Ross-Driscoll K", 
    # "Patzer RE", 
    # "Kirk AD", 
    "Matsouaka RA", 
    "Bhavsar NA", 
    "McElroy LM",
    # "Locke JE",
    "Milam AJ"
  )

#############
# Clean Data
#############

# Clean pub data
pubs <- 
  pubs %>%
  #Remove trailing period after last author
  mutate(Authors = stringr::str_remove(Authors, "\\.$")) %>%
  
  #Filter out incorrect pubs for "Adams A" and "Adams AC"
  filter(!grepl("Adams AC", Authors)) %>%
  filter(!(grepl("Adams A\\,", Authors) & !grepl("[Tt]ransplant", Title))) %>%
  
  #Modify inconsistent ones to match the list of target author names above.
  mutate(Authors = stringr::str_replace(Authors, "McElroy L([\\,$])", "McElroy LM\\1"),
         Authors = stringr::str_replace(Authors, "Kirk A([\\,$])", "Kirk AD\\1"),
         Authors = stringr::str_replace(Authors, "Ross KH([\\,$])", "Ross-Driscoll K\\1"),
         Authors = stringr::str_replace(Authors, "Ross K([\\,$])", "Ross-Driscoll K\\1"),
         Authors = stringr::str_replace(Authors, "Ross-Driscoll KH([\\,$])", "Ross-Driscoll K\\1"),
         Authors = stringr::str_replace(Authors, "Bhavsar N([\\,$])", "Bhavsar NA\\1"),
         Authors = stringr::str_replace(Authors, "Bhavsar NAA([\\,$])", "Bhavsar NA\\1"),
         Authors = stringr::str_replace(Authors, "Strauss A([\\,$])", "Strauss AT\\1"),
         Authors = stringr::str_replace(Authors, "Taber D([\\,$])", "Taber DJ\\1"),
         Authors = stringr::str_replace(Authors, "Adams A([\\,$])", "Adams AB\\1"),
         Authors = stringr::str_replace(Authors, "Matsouaka R([\\,$])", "Matsouaka RA\\1"),
         Authors = stringr::str_replace(Authors, "Caicedo J([\\,$])", "Caicedo-Ramirez JC\\1"),
         Authors = stringr::str_replace(Authors, "Caicedo J([\\,$])", "Caicedo-Ramirez JC\\1"),
         Authors = stringr::str_replace(Authors, "Caicedo JC([\\,$])", "Caicedo-Ramirez JC\\1"),
         Authors = stringr::str_replace(Authors, "Reed R([\\,$])", "Reed RD\\1"),
         Authors = stringr::str_replace(Authors, "Deierhoi Reed R([\\,$])", "Reed RD\\1"),
         Authors = stringr::str_replace(Authors, "Deierhoi Reed RD([\\,$])", "Reed RD\\1"),
         Authors = stringr::str_replace(Authors, "Schold J([\\,$])", "Schold JD\\1"),
         Authors = stringr::str_replace(Authors, "Harding J([\\,$])", "Harding JF\\1")
  ) %>%
  
  #Filter to only unique pubs
  filter(!duplicated(Title)) %>%
  
  #Remove when separated from last individual author by ";"
  mutate(Authors = stringr::str_remove(Authors, "\\; [A-Za-z0-9 \\-\\-\\,\\.\\'\\’\\/\\(\\)\\*\\s]+$"),  #With end of line following
         Authors = stringr::str_remove(Authors, "\\; [A-Za-z0-9 \\-\\-\\,\\.\\'\\’\\/\\(\\)\\*\\s]+\\;") #With another non-individual author following or amongst individual authors
  ) %>%
  
  #Remove specific patterns that lack a ";" separating from last author
  mutate(Authors = stringr::str_remove(Authors, " American Society of Health-System Pharmacists .+"),
         Authors = stringr::str_remove(Authors, "; Million Veteran Program"),
         Authors = stringr::str_remove(Authors, "; Penn Medicine BioBank"),
         Authors = stringr::str_remove(Authors, "Geisinger-Regeneron DiscovEHR Collaboration; EPoS Consortium; VA Million Veteran Program")
  )

########################
# Prepare Network Inputs
########################

#Generate a list where each pub is an item that contains a character vector of co-authors
coauthors <- sapply(as.character(pubs$Authors), strsplit, ", ")

#Name the list items using the pub names
if(
  length(coauthors) == length(unique(pubs$Title))
) {
  names(coauthors) <- pubs$Title
} else {
  stop("Length of unique pubs does not match length of coauthors list")
}

#Trim whitespace
coauthors <- lapply(coauthors, trimws)

#Create a list of unique authors (nodes)
coauthors.uniq <- unique(unlist(coauthors))[order(unique(unlist(coauthors)))]

#Filter the unique author list to just the primary CHART co-authors
coauthors.uniq <-
  coauthors.uniq[coauthors.uniq %in% primary.authors]

#Create bipartite edges where columns are pubs and row are authors
#Cells indicate whether author is included in each pub
bipart.edges <- 
  foreach::foreach(i = 1:length(coauthors), .combine = 'cbind') %do% {
    coauthors.uniq %in% coauthors[[i]]
  }
rownames(bipart.edges) <- coauthors.uniq

#Convert bipartite edges to unimodal edges via matrix multiplication
uni.edges <- bipart.edges %*% t(bipart.edges) / 2

#Create a network object from the unimodal edges
author.statnet <- 
  as.network(uni.edges, 
             directed = FALSE,        #Co-authorships are undirected
             names.eval = "edge.lwd", #Edge weights are # of co-authored pubs
             ignore.eval = FALSE)

###########################
# Prepare VisNetwork Inputs
###########################

#Dataframe of author nodes and 
author.nodes <- 
  #Extract metadata from network object
  data.frame(id = 1:length(network::get.vertex.attribute(author.statnet,"vertex.names")),
             label = network::get.vertex.attribute(author.statnet,"vertex.names"),
             title = network::get.vertex.attribute(author.statnet,"vertex.names"),
             size = network::get.vertex.attribute(author.statnet,"size")/ 20,
             mass = (diag(uni.edges))
  ) %>%
  
  #Assign groups based on institution
  mutate(
    group = case_when(title %in% c("Matsouaka RA", "McElroy LM", "Bhavsar NA", "Kirk AD", "Rogers U", "Schappe T") ~ "Duke University",
                      title == "Caicedo-Ramirez JC" ~ "Northwestern University",
                      title == "Adams AB" ~ "University of Minnesota",
                      title == "Gordon EJ" ~ "Vanderbilt University",
                      title == "Harding JF" ~ "Emory University",
                      title == "Ng YH" ~ "University of New Mexico",
                      title %in% c("Patzer RE", "Ross-Driscoll K") ~ "Indiana University",
                      title %in% c("Reed RD", "Locke JE") ~ "University of Alabama at Birmingham",
                      title == "Schold JD" ~ "University of Colorado",
                      title == "Serper M" ~ "University of Pennsylvania",
                      title == "Strauss AT" ~ "Johns Hopkins University",
                      title == "Taber DJ" ~ "Medical University of South Carolina",
                      title == "Milam AJ" ~ "Mayo Clinic"
    ),
  
    #Assign colors using institution colors
    color = case_when(group == "Duke University" ~ color.duke,
                      group == "Northwestern University" ~ color.northwestern,
                      group == "University of Minnesota" ~ color.umn,
                      group == "Vanderbilt University" ~ color.vumc,
                      group == "Emory University" ~ color.emory,
                      group == "University of New Mexico" ~ color.newmexico,
                      group == "Indiana University" ~ color.indiana,
                      group == "University of Alabama at Birmingham" ~ color.uab,
                      group == "University of Colorado" ~ color.colorado,
                      group == "University of Pennsylvania" ~ color.penn,
                      group == "Johns Hopkins University" ~ color.hopkins,
                      group == "Medical University of South Carolina" ~ color.musc,
                      group == "Mayo Clinic" ~ color.mayo
    )
  ) %>%
  
  #SET NODE ORDER
  #Arrange by group (institution) then author last name
  arrange(group, title)

#Dataframe of edges
author.edges <- 
  #Extract edges from network object
  as.edgelist(author.statnet,
              attrname = "edge.lwd", #Extract edge width
              output = "tibble"
  ) %>%
  
  #Rename to conform to visNetwork
  rename(from = ".tail",
         to = ".head",
         width = "edge.lwd"          #Pass edge width attr
  )

########################
# App
########################

# Define UI for application that draws a vizNetwork plot
ui <- fluidPage(

    # Application title
    titlePanel("CHART Core Co-Authorship Network"),
    
    # View the network plot outside of any panel
    absolutePanel(visNetworkOutput("network",
                                   width = "100%",
                                   height = "100%"),
                  top = "60px",
                  height = "800px",
                  left = "30px",
                  width = "800px"
    )
)

# Define server logic required to load network data and build the plot
server <- function(input, output) {

    output$network <- renderVisNetwork({
      
      
      ###################
      # Build the network
      ###################
      visNetwork(
        author.nodes, 
        author.edges
        # height = "800px"
        # main = "CHART Consortium Co-Author Network", 
        # width = "100%",
      ) %>%
        visIgraphLayout(layout = "layout_in_circle", #Start nodes in a circle
                        type = "full"
        ) %>%
        visNodes(
          shape = "dot",
          color = list(
            background = "#0085AF",
            border = "#013848",
            highlight = "#FF8000"
          ),
          shadow = list(enabled = TRUE,
                        size = 10
          )
        ) %>%
        visEdges(
          shadow = FALSE,
          color = list(color = "#0085AF", 
                       highlight = "#C62F4B")
        ) %>%
        visOptions(
          nodesIdSelection = list(enabled  = TRUE, 
                                  useLabels = TRUE, 
                                  main = "Select by Author")
        ) %>%
        visLayout(randomSeed = 2) #Random seed does nothing because nodes ordered by 'title' but is required for plotting
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
